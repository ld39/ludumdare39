package gameobjects.ground;

import com.mygdx.ld39.Constants;

/**
 * Created by drmargarido on 29-07-2017.
 */
public class GroundFactory {
    public static Ground getGround(GroundType groundType, int initialX, int initialY)
    {
        int pixels_by_tile = Constants.MONITOR_WIDTH / (Constants.TILES_IN_SCREEN);
        switch (groundType)
        {
            case BASIC:
                return new Ground("basic_ground.png", initialX, initialY,
                       pixels_by_tile, Constants.TILE_HEIGHT);
            case BRICK1:
                return new Ground("red_brick1_edge_top.png", initialX, initialY,
                       pixels_by_tile, Constants.TILE_HEIGHT);
            case BRICK2:
                return new Ground("red_brick2_edge_top.png", initialX, initialY,
                       pixels_by_tile, Constants.TILE_HEIGHT);
            case SPECIAL:
                return new SpecialGround("floor.png", initialX, initialY,
                        1300, 120);
            default:
                return new Ground("basic_ground.png", initialX, initialY,
                       pixels_by_tile, Constants.TILE_HEIGHT);
        }
    }
}
