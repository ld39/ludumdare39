package gameobjects.ground;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.ld39.Constants;
import gameobjects.GameObject;
import gameobjects.GravityObject;
import gameobjects.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by drmargarido on 29-07-2017.
 */
public class GroundManager {
    private List<Ground> grounds;
    private List<GameObject> colliders;
    private Player player;

    public GroundManager(Player player) {
        this.player = player;
        this.grounds = new ArrayList<Ground>();

        int pixels_by_tile = 1300;
        for(int i=0; i < Constants.TILES_NUMBER; i++)
        {
            grounds.add(GroundFactory.getGround(GroundType.SPECIAL, i * pixels_by_tile, Constants.TILE_BOTTOM_DRAWING_POSITION));
        }

        colliders = new ArrayList<GameObject>();
    }

    public void update(float deltaTime) {
        if(player.isInCenterAndMoving()) {
            for (Ground ground : grounds)
                ground.update(deltaTime);
        }

        // Manage grounds
        Ground leftestGround = grounds.get(0);
        if(leftestGround.getContainer().getX() + leftestGround.getContainer().getWidth() < 0)
        {
            leftestGround.dispose();
            grounds.remove(leftestGround);

            Ground lastGround = grounds.get(grounds.size() - 1);
            int newGroundX = (int) (lastGround.getContainer().getX() + lastGround.getContainer().getWidth());
            grounds.add(GroundFactory.getGround(GroundType.SPECIAL, newGroundX, Constants.TILE_BOTTOM_DRAWING_POSITION));
        }

        // Check collision of objects with gravity with the ground
        for(GameObject gameObject: colliders)
        {
            for(Ground ground: grounds) {
                // If a collision is found put the gameObject at the top of ground and pass to the next one
                if(gameObject.getContainer().overlaps(ground.getContainer())) {
                    gameObject.getContainer().setY(ground.getContainer().getY() + ground.getContainer().getHeight());
                    gameObject.setVelocityY(0);

                    if(gameObject instanceof Player) {
                        ((Player) gameObject).setIsJumping(false);
                    }

                    break;
                }
            }
        }
    }

    public void render(SpriteBatch spriteBatch) {
        for(Ground ground: grounds)
            ground.render(spriteBatch);
    }

    public void dispose() {
        for(Ground ground: grounds)
            ground.dispose();
    }

    public void registerCollider(GameObject gameObject) {
        colliders.add(gameObject);
    }

    public void removeCollider(GameObject gameObject){
        colliders.remove(gameObject);
    }
}
