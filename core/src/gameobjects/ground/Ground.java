package gameobjects.ground;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.ld39.Constants;
import gameobjects.GameObject;

/**
 * Created by drmargarido on 29-07-2017.
 */
public class Ground extends GameObject {
    protected TextureRegion groundTexture;

    public Ground(String textureName, int initialX, int initialY, int width, int height){
        container = new Rectangle(initialX, initialY, width, height);
        groundTexture = new TextureRegion(new Texture(textureName));
    }

    @Override
    public void update(float deltaTime) {
        container.setX(container.getX() - Constants.PLAYER_MOVEMENT_VELOCITY * deltaTime);
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(groundTexture, container.getX(), container.getY(),
                container.getWidth(), container.getHeight());
    }

    @Override
    public void dispose() {
        groundTexture.getTexture().dispose();
    }

    public Rectangle getContainer() {
        return container;
    }
}
