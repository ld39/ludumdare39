package gameobjects.ground;

/**
 * Created by drmargarido on 29-07-2017.
 */
public enum GroundType {
    BASIC,
    BRICK1,
    BRICK2,
    SPECIAL
}
