package gameobjects.ground;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by drmargarido on 31-07-2017.
 */
public class SpecialGround extends Ground {
    private Rectangle drawingContainer;

    public SpecialGround(String textureName, int initialX, int initialY, int width, int height) {
        super(textureName, initialX, initialY, width, height / 2);
        drawingContainer = new Rectangle(initialX, initialY, width, height);
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        drawingContainer.setX(container.getX());
        drawingContainer.setY(container.getY());

        spriteBatch.draw(groundTexture, drawingContainer.getX(), drawingContainer.getY(),
                drawingContainer.getWidth(), drawingContainer.getHeight());
    }
}
