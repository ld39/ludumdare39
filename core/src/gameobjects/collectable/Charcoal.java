package gameobjects.collectable;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.ld39.Constants;
import gameobjects.Player;

import java.util.Random;

/**
 * Created by drmargarido on 31-07-2017.
 */
public class Charcoal extends Collectable{
    private Rectangle drawingContainer;

    public Charcoal(int initialX, int initialY) {
        super(getRandomCharcoalTexture(), initialX, initialY, 22, 16);
        drawingContainer = new Rectangle(initialX, initialY - 10, 22, 16);
    }

    private static String getRandomCharcoalTexture()
    {
        // Define random charcoal image
        String textureName = null;
        Random random = new Random();
        switch (random.nextInt(4))
        {
            case 0:
                textureName = "charcoal1.png";
                break;
            case 1:
                textureName = "charcoal2.png";
                break;
            case 2:
                textureName = "charcoal3.png";
                break;
            case 3:
                textureName = "charcoal4.png";
                break;
        }

        return textureName;
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        drawingContainer.setX(container.getX());
        drawingContainer.setY(container.getY() - 10);

        spriteBatch.draw(textureRegion, drawingContainer.getX(), drawingContainer.getY(),
                drawingContainer.getWidth(), drawingContainer.getHeight());
    }

    @Override
    public void pick(Player player) {
        player.addEnergy(Constants.CHARCOAL_ENERGY);
    }
}
