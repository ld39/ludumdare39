package gameobjects.collectable;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.ld39.Constants;
import gameobjects.Player;
import gameobjects.ground.GroundManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by drmargarido on 31-07-2017.
 */
public class CollectableManager {
    private List<Collectable> collectables;
    private Player player;
    private GroundManager groundManager;

    public CollectableManager(Player player, GroundManager groundManager)
    {
        collectables = new ArrayList<Collectable>();
        this.player = player;
        this.groundManager = groundManager;
    }

    public void update(float deltaTime) {
        for(Collectable collectable: collectables){
            collectable.update(deltaTime);
            if(player.isInCenterAndMoving())
                collectable.getContainer().setX(collectable.getContainer().getX() - Constants.PLAYER_MOVEMENT_VELOCITY * deltaTime);
        }

        List<Collectable> temporaryCollectables = new ArrayList<Collectable>(collectables);
        for(Collectable collectable: temporaryCollectables)
        {
            if(collectable.getContainer().overlaps(player.getContainer()))
            {
                collectable.pick(player);
                collectable.dispose();
                groundManager.removeCollider(collectable);
                collectables.remove(collectable);
                continue;
            }

            if(collectable.getContainer().getX() - collectable.getContainer().getWidth() < 0)
            {
                groundManager.removeCollider(collectable);
                collectables.remove(collectable);
                collectable.dispose();
            }
        }
    }

    public void render(SpriteBatch spriteBatch) {
        for(Collectable collectable: collectables)
            collectable.render(spriteBatch);
    }

    public void dispose(){
        for(Collectable collectable: collectables)
            collectable.dispose();
    }

    public void createCollectable(CollectableType collectableType, int initialX, int initialY){
        switch (collectableType)
        {
            case CHARCOAL:
                Collectable collectable = new Charcoal(initialX, initialY);
                collectables.add(collectable);
                groundManager.registerCollider(collectable);
                break;
        }
    }
}
