package gameobjects.collectable;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import gameobjects.GravityObject;
import gameobjects.Player;

/**
 * Created by drmargarido on 31-07-2017.
 */
public abstract class Collectable extends GravityObject{
    protected TextureRegion textureRegion;

    public Collectable(String textureName, int initialX, int initialY, int width, int height) {
        super(initialX, initialY, width, height);
        textureRegion = new TextureRegion(new Texture(textureName));
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(textureRegion, container.getX(), container.getY(),
                container.getWidth(), container.getHeight());
    }

    @Override
    public void dispose() {
        textureRegion.getTexture().dispose();
    }

    abstract public void pick(Player player);
}
