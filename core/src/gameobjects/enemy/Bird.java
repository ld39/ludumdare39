package gameobjects.enemy;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.ld39.Constants;

import java.util.Random;

/**
 * Created by drmargarido on 01-08-2017.
 */
public class Bird extends Enemy {


    private Animation<TextureRegion> flyingAnimation;
    private Animation<TextureRegion> dyingAnimation;
    private int startY;
    private boolean isMovingUp;

    private float stateTime;

    public Bird(int initialX, int initialY) {
        super(initialX, initialY, 74, 85);
        Random random = new Random();
        startY = initialY;
        getContainer().setY(initialY + random.nextInt(Constants.BIRD_HEIGHT_OFFSET * 2) - Constants.BIRD_HEIGHT_OFFSET);

        TextureRegion[][] flyingTextureRegions = TextureRegion.split(new Texture("bird-sprite.png"),
                148, 171);
        flyingAnimation = new Animation<TextureRegion>(0.1f, flyingTextureRegions[0]);

        TextureRegion[][] dyingTextureRegions = TextureRegion.split(new Texture("bird-dying-sprite.png"),
                148, 171);
        dyingAnimation = new Animation<TextureRegion>(0.1f, dyingTextureRegions[0]);

        stateTime = 0;
        isMovingUp = true;
    }

    @Override
    public void update(float deltaTime) {
        stateTime += deltaTime;

        if(isDeath)
            super.update(deltaTime);
        else{
            if(isMovingUp){
                getContainer().setY(getContainer().getY() + Constants.BIRD_HEIGHT_OFFSET * deltaTime);

                if(getContainer().getY() >= startY + Constants.BIRD_HEIGHT_OFFSET)
                    isMovingUp = false;
            } else {
                getContainer().setY(getContainer().getY() - Constants.BIRD_HEIGHT_OFFSET * deltaTime);

                if(getContainer().getY() <= startY - Constants.BIRD_HEIGHT_OFFSET){
                    isMovingUp = true;
                }
            }
            getContainer().setX(getContainer().getX() - Constants.ENEMY_SPEED * deltaTime);
        }

    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        TextureRegion currentTexture;

        if(isDeath)
            currentTexture = dyingAnimation.getKeyFrame(stateTime, false);
        else
            currentTexture = flyingAnimation.getKeyFrame(stateTime, true);

        spriteBatch.draw(currentTexture, container.getX(), container.getY(),
                container.getWidth(), container.getHeight());
    }

    @Override
    public void dispose() {
        for(TextureRegion textureRegion: dyingAnimation.getKeyFrames())
            textureRegion.getTexture().dispose();

        for(TextureRegion textureRegion: flyingAnimation.getKeyFrames())
            textureRegion.getTexture().dispose();
    }

    @Override
    public void hitByShot() {
        if(!isDeath) {
            isDeath = true;
            stateTime = 0;
        }
    }
}
