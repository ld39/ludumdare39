package gameobjects.enemy;

import gameobjects.GravityObject;

/**
 * Created by drmargarido on 31-07-2017.
 */
public abstract class Enemy extends GravityObject {
    protected boolean isDeath;

    public Enemy(int initialX, int initialY, int width, int height) {
        super(initialX, initialY, width, height);
        isDeath = false;
    }

    abstract public void hitByShot();
    public boolean isDeath() {
        return isDeath;
    }
}
