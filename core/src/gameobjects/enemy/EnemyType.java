package gameobjects.enemy;

/**
 * Created by drmargarido on 01-08-2017.
 */
public enum EnemyType {
    DOG,
    BIRD,
    OBSTACLE1,
    OBSTACLE2
}
