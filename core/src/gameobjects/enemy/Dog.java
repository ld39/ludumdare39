package gameobjects.enemy;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.ld39.Constants;
import gameobjects.GravityObject;

/**
 * Created by drmargarido on 30-07-2017.
 */
public class Dog extends Enemy {
    private Animation<TextureRegion> animation;
    private float stateTime;
    private Rectangle drawingContainer;
    private Animation<TextureRegion> deathAnimation;

    public Dog(int initialX, int initialY) {
        super(initialX + 10, initialY, 75, 90);
        drawingContainer = new Rectangle(initialX, initialY, 105, 118);

        TextureRegion[][] textureRegions = TextureRegion.split(new Texture("dog-sprite.png"),
                193, 216);
        animation = new Animation<TextureRegion>(0.1f, textureRegions[0]);

        TextureRegion[][] deathTextureRegions = TextureRegion.split(new Texture("dog-dying-sprite.png"),
                193, 216);
        deathAnimation = new Animation<TextureRegion>(0.05f, deathTextureRegions[0]);
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        stateTime += deltaTime;

        if(!isDeath)
            container.setX(container.getX() - Constants.ENEMY_SPEED * deltaTime);
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        drawingContainer.setX(container.getX() - 10);
        drawingContainer.setY(container.getY());

        TextureRegion activeRegion;
        if(isDeath)
            activeRegion = deathAnimation.getKeyFrame(stateTime, false);
        else
            activeRegion = animation.getKeyFrame(stateTime, true);

        spriteBatch.draw(activeRegion, drawingContainer.getX(), drawingContainer.getY(),
                drawingContainer.getWidth(), drawingContainer.getHeight());
    }

    @Override
    public void dispose() {
        for(TextureRegion textureRegion : animation.getKeyFrames())
            textureRegion.getTexture().dispose();

        for(TextureRegion textureRegion : deathAnimation.getKeyFrames())
            textureRegion.getTexture().dispose();
    }

    @Override
    public void hitByShot() {
        if(!isDeath) {
            isDeath = true;
            stateTime = 0;
        }
    }
}
