package gameobjects.enemy;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.ld39.Constants;
import gameobjects.Player;
import gameobjects.ground.GroundManager;
import states.PlayState;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by drmargarido on 31-07-2017.
 */
public class EnemyManager {
    private List<Enemy> objectList;
    private PlayState playState;
    private GroundManager groundManager;
    private Player player;
    private float lastEnemyTime;
    private float randomEnemyDelay;
    private EnemyType [] currentSpawnableElements;

    // Spawn groups over time
    static private EnemyType [] SPAWNABLE_ELEMENTS_1 = {
            EnemyType.DOG, EnemyType.DOG, EnemyType.DOG, EnemyType.OBSTACLE1
    };

    static private EnemyType [] SPAWNABLE_ELEMENTS_2 = {
            EnemyType.DOG, EnemyType.DOG, EnemyType.OBSTACLE2, EnemyType.OBSTACLE1
    };

    static private EnemyType [] SPAWNABLE_ELEMENTS_3 = {
            EnemyType.DOG, EnemyType.BIRD, EnemyType.OBSTACLE2, EnemyType.OBSTACLE1
    };

    static private EnemyType [] SPAWNABLE_ELEMENTS_4 = {
            EnemyType.DOG, EnemyType.BIRD, EnemyType.OBSTACLE2, EnemyType.OBSTACLE1, EnemyType.BIRD
    };


    public EnemyManager(PlayState playState, Player player, GroundManager groundManager)
    {
        objectList = new ArrayList<Enemy>();
        this.playState = playState;
        this.groundManager = groundManager;
        this.player = player;

        lastEnemyTime = 0;

        randomEnemyDelay = (float) (Math.random() * Constants.RANDOM_VARIATION_BETWEEN_ENEMIES_SPAWN);
        currentSpawnableElements = SPAWNABLE_ELEMENTS_1;
    }

    public void update(float deltaTime) {
        lastEnemyTime += deltaTime;

        for(Enemy enemy: objectList) {
            enemy.update(deltaTime);
            if(player.isInCenterAndMoving())
                enemy.getContainer().setX(enemy.getContainer().getX() - Constants.PLAYER_MOVEMENT_VELOCITY * deltaTime);
        }

        for(Enemy enemy: objectList)
        {
            if(!enemy.isDeath()) {
                if (player.getContainer().overlaps(enemy.getContainer())) {
                    playState.setGameOver();
                }
            }
        }

        // Remove enemy if out of the monitor
        List<Enemy> temporaryEnemyList = new ArrayList<Enemy>(objectList);
        for(Enemy enemy: temporaryEnemyList)
        {
            if(enemy.getContainer().getX() + enemy.getContainer().getWidth() <= 0) {
                enemy.dispose();
                groundManager.removeCollider(enemy);
                objectList.remove(enemy);
            }
        }

        if(lastEnemyTime >= Constants.MINIMUM_TIME_BETWEEN_ENEMIES_SPAWN + randomEnemyDelay && player.isInCenterAndMoving()) {
            Random random = new Random();
            EnemyType randomType = currentSpawnableElements[random.nextInt(currentSpawnableElements.length)];
            Enemy enemy = null;
            switch (randomType) {
                case DOG:
                    enemy = new Dog(Constants.MONITOR_WIDTH, 40);
                    break;
                case BIRD:
                    enemy = new Bird(Constants.MONITOR_WIDTH, 75);
                    break;
                case OBSTACLE1:
                    enemy = new Obstacle(1, Constants.MONITOR_WIDTH, 40);
                    break;
                case OBSTACLE2:
                    enemy = new Obstacle(2, Constants.MONITOR_WIDTH, 40);
                    break;
            }
            groundManager.registerCollider(enemy);
            objectList.add(enemy);

            lastEnemyTime = 0;
            randomEnemyDelay = (float) (Math.random() * Constants.RANDOM_VARIATION_BETWEEN_ENEMIES_SPAWN);
        }
    }

    public void render(SpriteBatch spriteBatch) {
        for(Enemy enemy: objectList)
            enemy.render(spriteBatch);
    }

    public void dispose() {
        for(Enemy enemy: objectList)
            enemy.dispose();
    }

    public List<Enemy> getEnemies(){
        return objectList;
    }

    public void setSpawnableElements(int id){
        switch (id){
            case 1:
                 currentSpawnableElements = SPAWNABLE_ELEMENTS_1;
                 break;
            case 2:
                currentSpawnableElements = SPAWNABLE_ELEMENTS_2;
                break;
            case 3:
                currentSpawnableElements = SPAWNABLE_ELEMENTS_3;
                break;
            case 4:
                currentSpawnableElements = SPAWNABLE_ELEMENTS_4;
                break;
            default:
                break;
        }
    }
}
