package gameobjects.enemy;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by drmargarido on 01-08-2017.
 */
public class Obstacle extends Enemy {
    private TextureRegion texture;
    private Rectangle drawingContainer;

    public Obstacle(int obstacleType, int initialX, int initialY) {
        super(initialX, initialY - 10, 55, 80);
        drawingContainer = new Rectangle(initialX - 5, initialY - 10, 75, 93);

        switch (obstacleType)
        {
            case 1:
                texture = new TextureRegion(new Texture("obstacle1.png"));
                break;
            case 2:
                texture = new TextureRegion(new Texture("obstacle2.png"));
                break;
            default:
                System.out.println("Obstacle type " + obstacleType + " not defined!");
        }

    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        drawingContainer.setX(container.getX() - 5);

        spriteBatch.draw(texture, drawingContainer.getX(), drawingContainer.getY(),
                drawingContainer.getWidth(), drawingContainer.getHeight());
    }

    @Override
    public void dispose() {
        texture.getTexture().dispose();
    }

    @Override
    public void hitByShot() {

    }
}
