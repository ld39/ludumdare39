package gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.ld39.Constants;
import states.PlayState;

/**
 * Created by drmargarido on 29-07-2017.
 */
public class Player extends GravityObject{
    private static final float TRANSITION_ANIMATION_TIME = 0.04f;
    private static final float DEATH_ANIMATION_TRANSITION_FRAME_TIME = 0.1f;
    private static final float SOUND_AFTER_STANDING_TIME = 1.5f;
    private static final float JUMP_TRANSITION_TIME = 0.2f;
    private int energy;

    private Animation<TextureRegion> animation;
    private TextureRegion transitionTexture;
    private TextureRegion standingTexture;
    private Animation<TextureRegion> deathAnimation;
    private Animation<TextureRegion> jumpAnimation;
    private Animation<TextureRegion> fallAnimation;
    private Animation<TextureRegion> inAirAnimation;

    private enum JumpState {
        START_JUMP,
        IN_AIR,
        END_JUMP,
        ON_GROUND
    }
    private JumpState jumpState;

    private float stateTime;
    private Rectangle drawingContainer;
    private float transitionTime;
    private float standingTime;
    private float jumpTime;
    private float timeSinceLastShot;

    private boolean isStanding;
    private boolean isInTransition;
    private boolean isJumping;
    private boolean isMovingForward;
    private boolean isMovingBackward;
    private boolean isDeath;

    private Sound jump;
    private Sound shot;
    private Music run;

    private PlayState playState;

    public Player(PlayState playState) {
        super(20, 100, 85, 160);
        drawingContainer = new Rectangle(20, 100, 135, 171);

        stateTime = 0;
        transitionTime = 0;
        standingTime = 0;
        jumpTime = 0;
        timeSinceLastShot = 0;

        energy = Constants.PLAYER_ENERGY;

        isJumping = false;
        jumpState = JumpState.ON_GROUND;
        isMovingForward = false;
        isMovingBackward = false;

        jump = Gdx.audio.newSound(Gdx.files.internal("salto.ogg"));
        run = Gdx.audio.newMusic(Gdx.files.internal("bep_bop.ogg"));
        shot = Gdx.audio.newSound(Gdx.files.internal("tiro.ogg"));
        run.setVolume(1.5f);

        standingTexture = new TextureRegion(new Texture("robot-stand.png"));
        transitionTexture = new TextureRegion(new Texture("robot-stand2.png"));

        // Load frames for the dying animation
        TextureRegion[][] dyingTextureRegions = TextureRegion.split(new Texture("robot-dying-sprite.png"),
                270, 342);
        deathAnimation = new Animation<TextureRegion>(DEATH_ANIMATION_TRANSITION_FRAME_TIME, dyingTextureRegions[0]);

        // Load frames for the jumping animation
        TextureRegion[][] jumpingTextureRegions = TextureRegion.split(new Texture("robot-jump-sprite.png"),
                270, 342);
        jumpAnimation = new Animation<TextureRegion>(DEATH_ANIMATION_TRANSITION_FRAME_TIME, jumpingTextureRegions[0]);

        TextureRegion [] fallingFrames = new TextureRegion[3];
        fallingFrames[0] = jumpingTextureRegions[0][1];
        fallingFrames[1] = jumpingTextureRegions[0][0];
        fallingFrames[2] = jumpingTextureRegions[0][1];

        fallAnimation = new Animation<TextureRegion>(DEATH_ANIMATION_TRANSITION_FRAME_TIME, fallingFrames);

        TextureRegion[][] inAirTextureRegions = TextureRegion.split(new Texture("robot-jump-air-sprite.png"),
                270, 342);
        inAirAnimation = new Animation<TextureRegion>(0.1f, inAirTextureRegions[0]);

        // Load frames for the walking animation
        TextureRegion[][] textureRegions = TextureRegion.split(new Texture("robot-sprite.png"),
                270, 342);
        animation = new Animation<TextureRegion>(0.1f, textureRegions[0]);

        isInTransition = false;
        isStanding = true;
        isDeath = false;

        this.playState = playState;
    }

    public void handleInput() {
        if(Gdx.input.isKeyPressed(Input.Keys.W) || Gdx.input.isKeyPressed(Input.Keys.UP)){
            if(!isJumping) {
                setIsJumping(true);
                velocityY = Constants.PLAYER_JUMP_SIZE;

                container.setY(container.getY() + 10);
                jump.play(0.08f);

                useEnergy(Constants.JUMP_ENERGY_COST);
            }
        }

        if(Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            if(container.getX() >= 0) {
                velocityX = -Constants.PLAYER_MOVEMENT_VELOCITY;
                isMovingBackward = true;
            }

            if(!run.isPlaying() && isStanding && standingTime >= SOUND_AFTER_STANDING_TIME)
            {
                run.play();
                standingTime = 0;
            }

            useEnergy(Constants.RUN_ENERGY_COST);
        }

        if(Gdx.input.isKeyPressed(Input.Keys.D) || Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            if(container.getX() + container.getWidth() <= Constants.MONITOR_WIDTH){
                if(!((container.getX() + container.getWidth() / 2) >= (Constants.MONITOR_WIDTH / 2)))
                    velocityX = Constants.PLAYER_MOVEMENT_VELOCITY;

                isMovingForward = true;
            }

            if(!run.isPlaying() && isStanding && standingTime >= SOUND_AFTER_STANDING_TIME)
            {
                run.play();
                standingTime = 0;
            }

            useEnergy(Constants.RUN_ENERGY_COST);
        }

        if(Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            if(timeSinceLastShot >= Constants.SHOT_DELAY) {
                // Add Shot in the gun limit position
                playState.addShot((int) (container.getX() + 110), (int) (container.getY() + 65));
                useEnergy(Constants.SHOT_ENERGY_COST);
                shot.play(0.5f);

                // Reset shot delay
                timeSinceLastShot = 0;
            }
        }
    }

    @Override
    public void update(float deltaTime) {
        stateTime += deltaTime;
        timeSinceLastShot += deltaTime;

        if(jumpState != JumpState.ON_GROUND) {
            jumpTime += deltaTime;
            if(jumpTime >= JUMP_TRANSITION_TIME)
            {
                if(jumpState == JumpState.START_JUMP)
                    jumpState = JumpState.IN_AIR;
                else if(jumpState == JumpState.END_JUMP)
                    jumpState = JumpState.ON_GROUND;
            }
        }

        if(isInTransition || isDeath)
            transitionTime += deltaTime;

        if(transitionTime >= TRANSITION_ANIMATION_TIME)
            isInTransition = false;

        if(isStanding)
            standingTime += deltaTime;

        super.update(deltaTime);

        drawingContainer.setX(container.getX());
        drawingContainer.setY(container.getY());
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        TextureRegion activeRegion = null;

        if(!isDeath) {
            if(jumpState == JumpState.ON_GROUND) {
                if (isMovingForward || isMovingBackward) {
                    // If the player was standing and now is running start the transition animation
                    if (isStanding) {
                        startTransition();
                        isStanding = false;
                    } else
                        activeRegion = animation.getKeyFrame(stateTime, true);
                } else {
                    // If the player was running and now is standing start the transition animation
                    if (!isStanding) {
                        startTransition();
                        isStanding = true;
                    } else
                        activeRegion = standingTexture;
                }

                if (isInTransition)
                    activeRegion = transitionTexture;
            }
            else {
                switch (jumpState)
                {
                    case START_JUMP:
                        activeRegion = jumpAnimation.getKeyFrame(jumpTime, false);
                        break;
                    case IN_AIR:
                        activeRegion = inAirAnimation.getKeyFrame(jumpTime, true);
                        break;
                    case END_JUMP:
                        activeRegion = fallAnimation.getKeyFrame(jumpTime, false);
                        break;
                }
            }
        }
        else {
            activeRegion = deathAnimation.getKeyFrame(transitionTime, false);
        }

        spriteBatch.draw(activeRegion, drawingContainer.getX(), drawingContainer.getY(),
                drawingContainer.getWidth(), drawingContainer.getHeight());
    }

    @Override
    public void dispose() {
        for(TextureRegion textureRegion : animation.getKeyFrames())
            textureRegion.getTexture().dispose();

        for(TextureRegion textureRegion: deathAnimation.getKeyFrames())
            textureRegion.getTexture().dispose();

        for(TextureRegion textureRegion: fallAnimation.getKeyFrames())
            textureRegion.getTexture().dispose();

        for(TextureRegion textureRegion: jumpAnimation.getKeyFrames())
            textureRegion.getTexture().dispose();

        for(TextureRegion textureRegion: inAirAnimation.getKeyFrames())
            textureRegion.getTexture().dispose();

        jump.dispose();
        run.dispose();
        shot.dispose();

        standingTexture.getTexture().dispose();
        transitionTexture.getTexture().dispose();
    }

    public void setIsJumping(boolean isJumping) {
        // Ignore it if the received isJumping is equals to the current isJumping
        if(isJumping == this.isJumping)
            return;

        if(isJumping) {
            this.isJumping = true;
            this.jumpState = JumpState.START_JUMP;
        }
        else {
            this.isJumping = false;
            this.jumpState = JumpState.END_JUMP;
        }

        jumpTime = 0;
    }

    public boolean isInCenterAndMoving()
    {
        return isMovingForward && (container.getX() + container.getWidth() / 2) > (Constants.MONITOR_WIDTH / 3);
    }

    public int getEnergy() {
        return energy;
    }

    /*
    * Reset the player movement flags and velocity
    * */
    public void resetPlayerMovement(){
        isMovingForward = false;
        isMovingBackward = false;
        velocityX = 0;
    }

    private void startTransition()
    {
        isInTransition = true;
        transitionTime = 0;
    }

    public void triggerPlayerDeath() {
        isDeath = true;
        transitionTime = 0;
    }

    private void useEnergy(int quantity)
    {
        energy -= quantity;

        if(energy < 0)
            energy = 0;
    }

    public void addEnergy(int quantity) {
        energy += quantity;
    }
}
