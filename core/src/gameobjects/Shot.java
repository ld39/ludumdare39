package gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.ld39.Constants;

/**
 * Created by drmargarido on 31-07-2017.
 */
public class Shot extends GameObject {
    private TextureRegion textureRegion;

    public Shot(int initialX, int initialY) {
        container = new Rectangle(initialX, initialY, 16, 9);
        textureRegion = new TextureRegion(new Texture("shot.png"));
    }

    @Override
    public void update(float deltaTime) {
        container.setX(container.getX() + Constants.SHOT_VELOCITY * deltaTime);
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        spriteBatch.draw(textureRegion, container.getX(), container.getY(),
                container.getWidth(), container.getHeight());
    }

    @Override
    public void dispose() {
        textureRegion.getTexture().dispose();
    }
}
