package gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.ld39.Constants;
import gameobjects.collectable.CollectableManager;
import gameobjects.collectable.CollectableType;
import gameobjects.enemy.Enemy;
import gameobjects.enemy.EnemyManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by drmargarido on 31-07-2017.
 */
public class ShotManager {
    private List<Shot> shotList;
    private EnemyManager enemyManager;
    private CollectableManager collectableManager;

    public ShotManager(EnemyManager enemyManager, CollectableManager collectableManager)
    {
        shotList = new ArrayList<Shot>();
        this.enemyManager = enemyManager;
        this.collectableManager = collectableManager;
    }

    public void update(float deltaTime)
    {
        for(Shot shot: shotList)
            shot.update(deltaTime);

        List<Shot> temporaryShotList = new ArrayList<Shot>(shotList);
        for(Shot shot: temporaryShotList){
            // Check collision with the enemies
            for(Enemy enemy: enemyManager.getEnemies())
            {
                if(!enemy.isDeath()) {
                    if (shot.container.overlaps(enemy.container)) {
                        shot.dispose();
                        shotList.remove(shot);
                        enemy.hitByShot();
                        if(enemy.isDeath())
                        {
                            collectableManager.createCollectable(CollectableType.CHARCOAL,
                                    ((int) enemy.getContainer().getX() + (int) enemy.getContainer().getWidth() / 2),
                                    ((int) enemy.getContainer().getY()));
                        }
                        break;
                    }
                }
            }

            // Check if out of monitor
            if(shot.getContainer().getX() >= Constants.MONITOR_WIDTH){
                shot.dispose();
                shotList.remove(shot);
            }
        }
    }

    public void render(SpriteBatch spriteBatch)
    {
        for(Shot shot: shotList)
            shot.render(spriteBatch);
    }

    public void dispose()
    {
        for(Shot shot: shotList)
            shot.dispose();
    }

    public void addShot(int initialX, int initialY)
    {
        shotList.add(new Shot(initialX, initialY));
    }
}
