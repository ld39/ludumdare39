package gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by drmargarido on 29-07-2017.
 */
public abstract class GameObject {
    protected Rectangle container;
    protected float velocityX, velocityY;

    abstract public void update(float deltaTime);
    abstract public void render(SpriteBatch spriteBatch);
    abstract public void dispose();

    public Rectangle getContainer() {
        return container;
    }

    public float getVelocityX() {
        return velocityX;
    }

    public void setVelocityX(float velocityX) {
        this.velocityX = velocityX;
    }

    public float getVelocityY() {
        return velocityY;
    }

    public void setVelocityY(float velocityY) {
        this.velocityY = velocityY;
    }
}
