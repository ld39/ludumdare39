package gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.ld39.Constants;

/**
 * Created by drmargarido on 29-07-2017.
 */
public abstract class GravityObject extends GameObject {
    public GravityObject(int initialX, int initialY, int width, int height)
    {
       container =  new Rectangle(initialX, initialY, width, height);
       velocityX = 0;
       velocityY = 0;
    }

    public void update(float deltaTime) {
        if(container.getY() + container.getHeight() >= 0)
            velocityY -= Constants.GRAVITY * deltaTime;

        container.setY(container.getY() + velocityY * deltaTime);
        container.setX(container.getX() + velocityX * deltaTime);
    }
}
