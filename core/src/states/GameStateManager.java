package states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by drmargarido on 29-07-2017.
 */
public class GameStateManager {

    private State currentState;

    public GameStateManager()
    {
        //currentState = new MenuState(this);
        currentState = new PlayState(this);
    }

    public void setState(State newState)
    {
        currentState.dispose();
        currentState = newState;
    }

    public State getCurrentState()
    {
        return currentState;
    }

    public void update(float deltaTime)
    {
        currentState.update(deltaTime);
    }

    public void render(SpriteBatch spriteBatch)
    {
        currentState.render(spriteBatch);
    }

    public void handleInput()
    {
        currentState.handleInput();
    }

    public void dispose()
    {
        currentState.dispose();
    }
}
