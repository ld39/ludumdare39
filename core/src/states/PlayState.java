package states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.ld39.Constants;
import gameobjects.Shot;
import gameobjects.ShotManager;
import gameobjects.collectable.CollectableManager;
import gameobjects.enemy.Dog;
import gameobjects.Player;
import gameobjects.enemy.EnemyManager;
import gameobjects.ground.GroundManager;

import java.util.List;

/**
 * Created by drmargarido on 29-07-2017.
 */
public class PlayState extends State {
    // 2 backgrounds bigger than the monitor, so we can use
    // the same texture and replace them without people noticing
    private Rectangle background;
    private Rectangle background2;
    private TextureRegion backgroundTexture;
    private TextureRegion titleTexture;

    private GroundManager groundManager;
    private EnemyManager enemyManager;
    private ShotManager shotManager;
    private CollectableManager collectableManager;
    private Player player;

    private boolean gameOver;

    private String gameOverText;
    private BitmapFont gameOverBitmapFont;

    private Music music;

    private int score;
    private float elapsedTime;

    public PlayState(GameStateManager gameStateManager) {
        super(gameStateManager);

        background = new Rectangle(0, 76, 1012, 405);
        background2 = new Rectangle(1012, 76, 1012, 405);
        backgroundTexture = new TextureRegion(new Texture("city.png"));
        titleTexture = new TextureRegion(new Texture("steambot.png"));

        player = new Player(this);
        groundManager = new GroundManager(player);
        groundManager.registerCollider(player);

        enemyManager = new EnemyManager(this, player, groundManager);
        collectableManager = new CollectableManager(player, groundManager);
        shotManager = new ShotManager(enemyManager, collectableManager);


        gameOver = false;

        gameOverText = "Game Over: Press R to restart!";
        gameOverBitmapFont = new BitmapFont();

        music = Gdx.audio.newMusic(Gdx.files.internal("background_music.ogg"));
        music.setLooping(true);
        music.setVolume(0.25f);
        music.play();

        score = 0;
        elapsedTime = 0;
    }

    @Override
    public void update(float deltaTime) {
        elapsedTime += deltaTime;

        if(player.isInCenterAndMoving()) {
            // background
            background.setX(background.getX() - Constants.BACKGROUND_SPEED * deltaTime);
            background2.setX(background2.getX() - Constants.BACKGROUND_SPEED * deltaTime);

            // If the background texture is out of the monitor pass it to the right side
            if (background.getX() + background.getWidth() < 0) {
                background.setX(background2.getX() + background.getWidth());

                // Define background2 as the primary background and send the first to the end of the monitor
                Rectangle auxiliaryBackground = background;
                background = background2;
                background2 = auxiliaryBackground;
            }
        }

        // If the game is over stop the enemies
        if(!gameOver)
            enemyManager.update(deltaTime);

        groundManager.update(deltaTime);
        shotManager.update(deltaTime);
        collectableManager.update(deltaTime);
        player.update(deltaTime);

        // If player has no energy end game
        if(player.getEnergy() <= 0) {
            Constants.ENEMY_SPEED = 100;
            enemyManager.setSpawnableElements(1);
            setGameOver();
        }

        if(player.isInCenterAndMoving()) {
            score += 1;
            if(score >= 1000 && score < 2000){
                Constants.ENEMY_SPEED = 150;
                enemyManager.setSpawnableElements(2);
            }
            else if(score >= 2000 && score < 3000) {
                Constants.ENEMY_SPEED = 180;
                enemyManager.setSpawnableElements(3);
            }
            else if(score >= 3000 && score < 5000) {
                Constants.ENEMY_SPEED = 210;
                enemyManager.setSpawnableElements(4);
            }
            else if(score >= 5000 && score < 8000)
                Constants.ENEMY_SPEED = 250;
            else if(score >= 8000 && score < 12500)
                Constants.ENEMY_SPEED = 300;
            else if(score >= 12500)
                Constants.ENEMY_SPEED = 350;
        }
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        // background
        spriteBatch.draw(backgroundTexture, background.x, background.y, background.width, background.height);
        spriteBatch.draw(backgroundTexture, background2.x, background2.y, background2.width, background2.height);

        // Ground
        groundManager.render(spriteBatch);
        enemyManager.render(spriteBatch);
        shotManager.render(spriteBatch);
        collectableManager.render(spriteBatch);
        player.render(spriteBatch);

        BitmapFont font = new BitmapFont();
        font.draw(spriteBatch, "Available Energy: " + player.getEnergy(), 20, Constants.MONITOR_HEIGHT - 20);
        font.draw(spriteBatch, "Score: " + score, Constants.MONITOR_WIDTH - 100, Constants.MONITOR_HEIGHT - 20);

        if(gameOver)
        {
            gameOverBitmapFont.setColor(1.0f, 1.0f, 1.0f, 1.0f);
            gameOverBitmapFont.draw(spriteBatch, gameOverText,
                    Constants.MONITOR_WIDTH / 2 - 100, Constants.MONITOR_HEIGHT - Constants.MONITOR_WIDTH / 4);
        }

        if(elapsedTime < 4) {
            spriteBatch.draw(titleTexture, Constants.MONITOR_WIDTH * 0.25f,
                    (float) (Constants.MONITOR_HEIGHT * 0.75), titleTexture.getRegionWidth(),
                    titleTexture.getRegionHeight());
        }
    }

    @Override
    public void dispose() {
        groundManager.removeCollider(player);
        groundManager.dispose();

        enemyManager.dispose();

        backgroundTexture.getTexture().dispose();
        player.dispose();
        music.dispose();

        gameOverBitmapFont.dispose();
        shotManager.dispose();

        collectableManager.dispose();

        titleTexture.getTexture().dispose();
    }

    @Override
    public void handleInput() {
        player.resetPlayerMovement();

        if(!gameOver)
            player.handleInput();

        if(Gdx.input.isKeyPressed(Input.Keys.R))
        {
            if(gameOver)
                gameStateManager.setState(new PlayState(gameStateManager));
        }
    }

    public void setGameOver()
    {
        if(!gameOver)
        {
            gameOver = true;
            music.setVolume(0.55f);
            player.triggerPlayerDeath();
        }
    }

    public void addShot(int initialX, int initialY)
    {
        shotManager.addShot(initialX, initialY);
    }
}
