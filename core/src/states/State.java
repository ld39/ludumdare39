package states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by drmargarido on 29-07-2017.
 */
public abstract class State {
    protected GameStateManager gameStateManager;

    public State(GameStateManager gameStateManager)
    {
        this.gameStateManager = gameStateManager;
    }

    public abstract void update(float deltaTime);
    public abstract void render(SpriteBatch spriteBatch);
    public abstract void dispose();
    public abstract void handleInput();
}