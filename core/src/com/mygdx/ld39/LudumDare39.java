package com.mygdx.ld39;

import states.GameStateManager;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LudumDare39 extends ApplicationAdapter {
	private SpriteBatch batch;
	private GameStateManager gameStateManager;

	@Override
	public void create () {
		batch = new SpriteBatch();
		gameStateManager = new GameStateManager();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.2f, 0.7f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		gameStateManager.handleInput();
		gameStateManager.update(Gdx.graphics.getDeltaTime());

		batch.begin();
		gameStateManager.render(batch);
		batch.end();
	}

	@Override
	public void dispose () {
		batch.dispose();
		gameStateManager.dispose();
	}
}
