package com.mygdx.ld39;

/**
 * Created by drmargarido on 29-07-2017.
 */
public class Constants {
    // Number of coordinates of movement by second
    public static final float BACKGROUND_SPEED = 60;

    // Add two more tiles than the ones you want to see on the screen
    // to make sure a empty spot doesn't appear when removing an old tile
    public static final int TILES_NUMBER = 2;
    public static final int TILES_IN_SCREEN = TILES_NUMBER - 1;

    public static final int TILE_HEIGHT = 80;
    public static final int TILE_BOTTOM_DRAWING_POSITION = -34;

    public static final int MONITOR_WIDTH = 640;
    public static final int MONITOR_HEIGHT = 480;
    public static final String TITLE = "STEAMBOT";

    public static final float GRAVITY = 500;

    public static final int PLAYER_JUMP_SIZE = 350;
    public static final int PLAYER_MOVEMENT_VELOCITY = 250;
    public static final int PLAYER_ENERGY = 2000;

    public static final float SHOT_DELAY = 0.1f;
    public static final float SHOT_VELOCITY = 500f;

    public static final int CHARCOAL_ENERGY = 400;
    public static final int RUN_ENERGY_COST = 1;
    public static final int JUMP_ENERGY_COST = 20;
    public static final int SHOT_ENERGY_COST = 40;

    public static final float MINIMUM_TIME_BETWEEN_ENEMIES_SPAWN = 2;
    public static final float RANDOM_VARIATION_BETWEEN_ENEMIES_SPAWN = 3;

    public static int ENEMY_SPEED = 100;

    public static final int BIRD_HEIGHT_OFFSET = 75;
}
