package com.mygdx.ld39.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.ld39.Constants;
import com.mygdx.ld39.LudumDare39;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = Constants.MONITOR_WIDTH;
		config.height = Constants.MONITOR_HEIGHT;
		config.title = Constants.TITLE;
		new LwjglApplication(new LudumDare39(), config);
	}
}
